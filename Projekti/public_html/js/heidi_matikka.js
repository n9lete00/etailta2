/* 
 Heidi Risto
 */
$(document).ready(function () {
    let laskurioikein = 0; //laskee oikeat vastaukset
    let laskun_numero = 1; //laskun numerointi aloittaa ykkösestä
    let matikkavisan_pituus = 0; //määrittää visan pituuden

    // satunnaislukugeneraattori:
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    
    //numeroalueet funktioon, jota voidaan kutsua myöhemmin eri click-funktioissa:
    let numero1 = 0;
    let numero2 = 0;
    function lukualue() {
        let valinta = Number($("[name=numerot]:checked").val());
        if (valinta === 1) {  // jos value = 1 numeroalue: 1-10
            numero1 = getRndInteger(1, 10);
            numero2 = getRndInteger(1, 10);
        }
        if (valinta === 2) {  // jos value = 2 numeroalue: 1-20
            numero1 = getRndInteger(1, 20);
            numero2 = getRndInteger(1, 20);
        }
        if (valinta === 3) {  // jos value = 3 numeroalue: 1-30
            numero1 = getRndInteger(1, 30);
            numero2 = getRndInteger(1, 30);
        }
    }
    

    $("#aloita_matikka").click(function () {
        matikkavisan_pituus = Number($("[name=määrät]:checked").val()); // luetaan visan pituus (value) "määrät"-valintanapeista
        $(this).addClass("piilossa"); //piilotetaan aloita -painike
        $("#numeroalue").addClass("piilossa"); //piilotetaan numeroalueen valintanapit
        $("#laskujenmäärä").addClass("piilossa"); //piilotetaan laskujen määrän valintanapit
        $(".matikkafontti").addClass("piilossa"); //piilotetaan otsikko myös
        $("h3.aloitus").addClass("piilossa"); //piilotetaan aloitus teksti ja nappi myös

        $("#vastaus").removeClass("piilossa"); // Tuo vastaus-laatikon näkösälle
        $("#vastaus").focus(); // vie kursorin suoraan vastauslaatikkoon
        lukualue(); // kutsutaan lukualue-funktiota

        $("#lasku").html(numero1 + " + " + numero2 + " = "); //tulostaa laskun
        $("#nro").html("<h3>Lasku " + laskun_numero + "/" + matikkavisan_pituus + "</h3>"); // tulostaa sivulle nro/5 tai nro/10 tai nro/15
        laskun_numero++;//laskunnumero +1
        $("#vastaa").removeClass("piilossa"); //tuo esille vastaa -napin
    });


    $("#vastaa").click(function () {
        let vastaus = Number($("#vastaus").val()); //lukee vastauslaatikkoon syötetyn numeron

        let tulos = numero1 + numero2;  //laskutoimitus
        if (vastaus === tulos) {
            //vastaus on oikein
            $("#tulostus").html("<h3>Oikein meni! Hienoa!</h3>");
            $("#vastaus").addClass(" oikein_matikka ");
            laskurioikein++;
        } else {
            //vastaus on väärin
            $("#tulostus").html("<h3>Voi, nyt meni väärin... oikea vastaus olisi ollut " + tulos + "." + "<br>Ehkä seuraavassa laskussa onnistut, tsemppiä!</h3>");
            $("#vastaus").addClass("väärin_matikka");
        }
        $("#vastaa").addClass("piilossa"); //piilottaa vastaa -napin
        $("#seuraava").removeClass("piilossa"); //tuo esille seuraava -napin
    });


    $("#seuraava").click(function () {
        $("#vastaus").val(""); //tyhjentää (vastaus-) input elementin
        $("#lasku").html(""); // tyhjentää laskun tulostuksen
        $("#tulostus").html(""); // tyhjentää tulostus -tekstin (oikein tai väärin)
        $("#vastaus").focus();  //kursori suoraan vastauslaatikkoon

        // visan pituutta määritellään tällä:
        if (laskun_numero <= matikkavisan_pituus) {
            lukualue();
            $("#lasku").html(numero1 + " + " + numero2 + " = "); //tulostaa uuden laskun
            $("#nro").html("<h3>Lasku " + laskun_numero + "/" + matikkavisan_pituus + "</h3>"); // tulostaa sivulle nro/5 tai nro/10 tai nro/15
            $("#vastaus").removeClass("oikein_matikka"); //muuttaa vastaustekstin takaisin mustaksi
            $("#vastaus").removeClass("väärin_matikka");  //muuttaa vastaustekstin takaisin mustaksi
            $("#vastaa").removeClass("piilossa"); //tuo näkösälle vastaa-napin
            $("#seuraava").addClass("piilossa"); //tuo esille seuraava -napin
            laskun_numero++; //laskunnumero +1
        } else {
            $("#vastaus").addClass("piilossa");
            $("#seuraava").addClass("piilossa"); //piilottaa vastaa -napin
            $("#uudestaan").removeClass("piilossa"); //tuo näkösälle uudestaan-napin
            $("#oikeat_vastaukset").removeClass("piilossa"); //tuo esille tekstin oikeista vastauksista
            $("#oikeat").html("Laskit oikein " + laskurioikein + " / " + matikkavisan_pituus + " laskusta"); // Näyttää oikein laskettujen laskujen määrän
            $("#nro").addClass("piilossa"); //piilotetaan lasku-laskuri
            
            // Palkitsemiset matikkavisan pituus huomioiden:
            if (matikkavisan_pituus === 5)
                if (laskurioikein === 5) {
                    $("#popUp").modal("show");
                    $("#video").get(0).play(); // play ei ole jqueryn functio joten siksi pitää hake Dom functio (get(0))
                } else if (laskurioikein === 4) {
                    $("#palkinto").html("<h3>Melkein kaikki oikein! Mahtava suoritus!</h3>" + "<br>" + '<img src="img/sydan.gif" alt=""/>');
                } else if (laskurioikein === 3) {
                    $("#palkinto").html("<h3>Melkein täydet pisteet, Hienoa! Haluatko yrittää vielä uudelleen?</h3>" + "<br>" + '<img src="img/patric.gif" alt=""/>');
                } else if (laskurioikein === 2) {
                    $("#palkinto").html("<h3>Melkein puolet oikein, hyvä! Tiedäthän, että harjoitus tekee mestarin?</h3>" + "<br>" + '<img src="img/kissa.gif" alt=""/>   ');
                } else if (laskurioikein === 1) {
                    $("#palkinto").html("<h3>Sait yhden oikein. Hyvä! Harjoittelemalla onnistut vielä paremmin.</h3>" + "<br>" + '<img src="img/minion.gif" alt=""/>   ');
                } else if (laskurioikein === 0) {
                    $("#palkinto").html("<h3>Voi harmin paikka, nyt taisivat ajatuksesi olla muualla? Haluatko kokeilla uudelleen?</h3>" + "<br>" + '<img src="img/vaarin.gif" alt=""/> ');
                }

            if (matikkavisan_pituus === 10)
                if (laskurioikein === 10) {
                    $("#popUp").modal("show");
                    $("#video").get(0).play(); // play ei ole jqueryn functio joten siksi pitää hakea Dom functio (get(0))
                } else if (laskurioikein <= 8) {
                    $("#palkinto").html("<h3>Melkein kaikki oikein! Mahtava suoritus!</h3>" + "<br>" + '<img src="img/sydan.gif" alt=""/>');
                } else if (laskurioikein <= 6) {
                    $("#palkinto").html("<h3>Melkein täydet pisteet, Hienoa! Haluatko yrittää vielä uudelleen?</h3>" + "<br>" + '<img src="img/patric.gif" alt=""/>');
                } else if (laskurioikein <= 4) {
                    $("#palkinto").html("<h3>Melkein puolet oikein, hyvä! Tiedäthän, että harjoitus tekee mestarin?</h3>" + "<br>" + '<img src="img/kissa.gif" alt=""/>   ');
                } else if (laskurioikein <= 2) {
                    $("#palkinto").html("<h3>Sait yhden oikein. Hyvä! Harjoittelemalla onnistut vielä paremmin.</h3>" + "<br>" + '<img src="img/minion.gif" alt=""/>   ');
                } else if (laskurioikein === 0) {
                    $("#palkinto").html("<h3>Voi harmin paikka, nyt taisivat ajatuksesi olla muualla? Haluatko kokeilla uudelleen?</h3>" + "<br>" + '<img src="img/vaarin.gif" alt=""/> ');
                }

            if (matikkavisan_pituus === 15)
                if (laskurioikein === 15) {
                    $("#popUp").modal("show");
                    $("#video").get(0).play(); // play ei ole jqueryn functio joten siksi pitää hake Dom functio (get(0))
                } else if (laskurioikein <= 12) {
                    $("#palkinto").html("<h3>Melkein kaikki oikein! Mahtava suoritus!</h3>" + "<br>" + '<img src="img/sydan.gif" alt=""/>');
                } else if (laskurioikein <= 9) {
                    $("#palkinto").html("<h3>Melkein täydet pisteet, Hienoa! Haluatko yrittää vielä uudelleen?</h3>" + "<br>" + '<img src="img/patric.gif" alt=""/>');
                } else if (laskurioikein <= 6) {
                    $("#palkinto").html("<h3>Melkein puolet oikein, hyvä! Tiedäthän, että harjoitus tekee mestarin?</h3>" + "<br>" + '<img src="img/kissa.gif" alt=""/>   ');
                } else if (laskurioikein <= 3) {
                    $("#palkinto").html("<h3>Sait yhden oikein. Hyvä! Harjoittelemalla onnistut vielä paremmin.</h3>" + "<br>" + '<img src="img/minion.gif" alt=""/>   ');
                } else if (laskurioikein === 0) {
                    $("#palkinto").html("<h3>Voi harmin paikka, nyt taisivat ajatuksesi olla muualla? Haluatko kokeilla uudelleen?</h3>" + "<br>" + '<img src="img/vaarin.gif" alt=""/> ');
                }
        }
    });
    
    // Tällä pelaaja voi aloittaa visan uudelleen
    $("#uudestaan").click(function () {
        location.reload();
    });


});
