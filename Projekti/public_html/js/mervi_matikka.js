/* 
 Mervi Heikkinen
 */

$(document).ready(function () {

    let laskuri = 1;//kysymysnumero/tehtävälkm
    let oikeinkpl = 0;//oikeat vastaukset/tehtävälkm
    let tehtlkm = 0;//tehtävien määrä
    let lukualue = 0;//valittu lukualue
    let erotus = 0; // oikea vastaus
    let vastaus = 0;
    let luku1 = 0; //satunnaisluku1
    let luku2 = 0; // satunnaisluku2

    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    //aloita-painikkeen funktio
    $("#start").click(function () {
        $("#valinnat").removeClass("not_visible"); //tuo esille laskutehtävän ja tehtävämäärän valinnat
        $(this).addClass("not_visible"); //piilottaa Aloita-painikkeen
        $("#esittely").addClass("not_visible");//piilottaa esittelytekstin
        $("#esittely2").addClass("not_visible");//piilottaa esittelytekstin 2

    });

    //radio-painikkeiden funktiot
    $(".choise").click(function () {
        let attribuutinArvo = $(this).attr("name");//määritetään muuttuja valitun painikkeen attribuutilla
        let valinta = "[name=" + attribuutinArvo + "]";//määritetään muuttuja "valinta" em. arvolla
        $(valinta).prop("disabled", true);//käytetään muuttujaa "valinta" poistamaan painikkeet, joilla em. attribuutti, käytöstä

        lukualue = Number($(valinta).attr("data-max"));//hakee valitun painikkeen arvon muuttujaan "lukualue"    
    });

    $(".choise2").click(function () {

        let attribuutinArvo = $(this).attr("name");//määritetään muuttuja valitun painikkeen attribuutilla
        let valinta2 = "[name=" + attribuutinArvo + "]";//määritetään muuttuja "valinta2" em. arvolla
        $(valinta2).prop("disabled", true);//käytetään muuttujaa "valinta2" poistamaan painikkeet, joilla em. attribuutti, käytöstä

        tehtlkm = Number($(valinta2).attr("data-max"));//hakee valitun painikkeen arvon muuttujaan "tehtävälkm"

        $("#seuraava").removeClass("not_visible");//näyttää seuraava-painikkeen
        $("#vastaus").select();//vie kohdistimen syöttökenttään
        $("#oikea").prop('disabled', true);//poistaa painikkeen "oikea vastaus" käytöstä
    });

    // seuraava -painikkeen funktio
    $("#seuraava").click(function () {
        $("#vastaus").val(""); // seuraava painikkeen painamisen tyhjentää vastauslaatikon
        $("#valinnat").addClass("not_visible");//piilottaa valinnat
        $(this).addClass("not_visible");//piilottaa seuraava-painikkeen

        $("#valinnat").removeClass("not_visible"); //tuo esille laskutehtävän ja tehtävämäärän valinnat
        let valinta = $("[name=lasku]:checked"); //luetaan haluttu lukualue
        let tehtävät = $("[name=tehtlkm]:checked"); //luetaan tehtävien haluttu lukumäärä
        let tehtlkm = Number($(tehtävät).attr("data-max"));

        let min = Number($(valinta).attr("data-min"));
        let max = Number($(valinta).attr("data-max"));
        luku1 = getRndInteger(1, max);
        luku2 = getRndInteger(1, max);
        if (luku1 >= luku2) {
            erotus = luku1 - luku2; // jos arvottu luku1 on yhtäsuuri tai suurempi kuin luku2 lasketaan tässä järjestyksessä
        } else {
            erotus = luku2 - luku1; // jos luku2 pienempi kuin luku1, kääntää luvut toisin päin 
        }
        if (vastaus === erotus) { //jos vastaus oikein
            oikeinkpl++;
        
        } else {  //jos vastaus väärin
        
        }
        $("#vahennettava").html(luku1); //tulostetaan laskutehtävä laatikkoon
        $("#vahentaja").html(luku2);
        $("#tarkista").removeClass("not_visible");
    });
    
//tarkista vastaus -painikkeen funktio
    $("#tarkista").click(function () { // tarkista painike ei toimi
        $("#seuraava").removeClass("not_visible");
        let vastaus = Number($("#vastaus").val());
        let erotus = luku1 - luku2; //oikea vastaus
        if (vastaus === erotus) { //jos vastaus oikein
            oikeinkpl++;
          
        } else {  //jos vastaus väärin
           
            $("#tulos").html("Oikea vastaus on " + erotus);
            $("#tulos").html(""); // tyhjentää tulos sarakkeen
            
        }
    });
//    
//    $("#lopputulos").click(function () {
//
//        let luku = Number($("#luku").val());
//        
//        if (luku === 10) {
//            $("#popUp").modal("show");
//            $("#video").get(0).play(); // play ei ole jqueryn functio joten siksi pitää hake Dom functio (get(0))
//        } else if (luku === 8) {
//            $("#palkinto").html("Tosi hyvä!" + "<br>" + '<img src="img/sydän.gif" alt=""/>');
//        } else if (luku === 7) {
//            $("#palkinto").html("Hienosti meni!" + "<br>" + '<img src="img/patric.gif" alt=""/>');
//        } else if (luku === 6) {
//            $("#palkinto").html("Hyvä!" + "<br>" + '<img src="img/kissa.gif" alt=""/>   ');
//        } else if (luku === 4) {
//            $("#palkinto").html("Lisää harjoitusta vain, kyllä se siitä!" + "<br>" + '<img src="img/minion.gif" alt=""/>   ');
//        } else if (luku === 0) {
//            $("#palkinto").html("Nyt ei mennyt ihan oikein. Yritäppä uudestaaan!" + "<br>" + '<img src="img/väärin.gif" alt=""/> ');
//        }
//
//    });
       

    //"aloita alusta"-painikkeen funktio
    $("#alusta").click(function () {
        location.reload();//lataa sivun uudelleen
    });
    
});
