/* 
 Created on : 31.3.2020, 11.33.47
 Author     : Teemu Leinonen
 */

//variablet sivun vaihtoon ja oikein/väärin laskuun
var page = 0;
var wrong = 0;
var right = 0;
//kysymykset
const questions = [
    {
        question: "Mikä näistä nisäkkäistä ei voi hypätä? ",
        explanation: "Kaikki elefantin jalkojen luut osoittaa alaspäin joten heillä ei ole ”jousta”, jota vaaditaan hyppyyn.",
        answers: [
            {text: "Kirahvi", correct: false},
            {text: "Elefantti", correct: true},
            {text: "Gorilla", correct: false},
            {text: "Siili", correct: false}
        ]
    },
    {
        question: "Mikä näistä on pitkäikäisin?",
        explanation: "Islanninsimpukalla on pitkäikäisimmän elävänä tavatun eläimen maailmanennätys 507 vuotta.",
        answers: [
            {text: "Grönlanninvalas", correct: false},
            {text: "Grönlanninhai", correct: false},
            {text: "Islanninsimpukka", correct: true},
            {text: "Jättiläiskilpikonna", correct: false}
        ]
    },
    {
        question: "Montako nenää etanalla on?",
        explanation: "Etanalla on neljä nenää.",
        answers: [
            {text: "1", correct: false},
            {text: "2", correct: false},
            {text: "3", correct: false},
            {text: "4", correct: true}
        ]
    },
    {
        question: "Mikä näistä on Suomen kansallishyönteinen?",
        explanation: "Suomen kansallishyönteinen on seitsenpistepirkko.",
        answers: [
            {text: "Seitsenpistepirkko", correct: true},
            {text: "Hyttynen", correct: false},
            {text: "Tupsukekomuurahainen", correct: false},
            {text: "Hirvikärpänen", correct: false}
        ]
    },
    {
        question: "Mikä näistä on nopein lintu? ",
        explanation: "Muuttohaukka on maailman nopein eläin ja voi pystysuorassa syöksyssä kiitää jopa 390 km/h.",
        answers: [
            {text: "Piikkipyrstökiitäjä", correct: false},
            {text: "Tunturihaukka", correct: false},
            {text: "Maakotka", correct: false},
            {text: "Muuttohaukka", correct: true}
        ]
    },
    {
        question: "Mikä näistä on nopein maaeläin?",
        explanation: "Gepardi on nopein maaeläin vauhdilla 110-120 km/h",
        answers: [
            {text: "Gepardi", correct: true},
            {text: "Hanka-antilooppi", correct: false},
            {text: "Quarterhevonen", correct: false},
            {text: "Juovagnuu", correct: false}
        ]
    },
    {
        question: "Mikä näistä on nopein kala?",
        explanation: "Nopein kala on mustamarliini vauhdilla 129 km/h.",
        answers: [
            {text: "Miekkakala", correct: false},
            {text: "Mustamarliini", correct: true},
            {text: "Purjekala", correct: false},
            {text: "Keltaevätonnikala", correct: false}
        ]
    },
    {
        question: "Millä näistä on paras kuulo?",
        explanation: "Isovahakoisa voi havaita jopa 300 kilohertsin taajuudet.",
        answers: [
            {text: "Isovahakoisa", correct: true},
            {text: "Lepakko", correct: false},
            {text: "Pöllö", correct: false},
            {text: "Koira", correct: false}
        ]

    }
];
//jquery koodi
$(document).ready(function () {
    //aloitus nappi, piilottaa alkusivun ja tulostaa kysymyksen ja vastausnapit funktiolla
    $("#start-btn").click(function () {
        $(this).addClass("hide");
        $("#start_text").addClass("hide");
        $("#arrows").addClass("hide");
        $("#question-container").removeClass("hide");
        printQuestion();
    });
    //tulostaa seuraavan kysymyksen sivun numeron perusteella question arraysta
    $("#next-btn").click(function () {
        page++;
        $(this).addClass("hide");
        printQuestion();
    });
    //piilottaa turhat ja tulostaa tuloksen
    $("#result-btn").click(function () {
        $("#result-btn").addClass("hide");
        $("#info").addClass("hide");
        $("#answer-buttons").html("");
        $("#explanation").addClass("hide");
        $("#question").addClass("hide");
        $("#question-container").html("");
        $("#score").removeClass("hide");
        $("#score").append("Vastasit oikein " + right + " / " + questions.length);
        //vaihtaa animaation kuvan pisteiden mukaan
        if (right === 0) {
            $(".anim-area").html('<i class="far fa-dizzy"></i>');
        } else if (right > 0 && right <= 2) {
            $(".anim-area").html('<i class="far fa-flushed"></i>');
        } else if (right > 2 && right <= 5) {
            $(".anim-area").html('<i class="far fa-grin-beam-sweat"></i>');
        } else if (right > 5 && right <= 7) {
            $(".anim-area").html('<i class="far fa-smile-beam"></i>');
        } else if (right === 8) {
            $(".anim-area").html('<i class="fas fa-grin-stars"></i>');
        }
    });
    //selvittää oliko clickattu vastaus oikein vai väärin, delegate että toimii myös tulostetuilla napeilla
    $("body").delegate("#answer", "click", function () {
        let answer = $(this).attr("value");
        if (questions[page].answers[answer]["correct"] === true) {
            right++;
            $(this).removeClass("btn-secondary");
            $(this).addClass("btn-success");
        } else {
            wrong++;
            $(this).removeClass("btn-secondary");
            $(this).addClass("btn-danger");
            $("#explanation").html(questions[page].explanation);
            for (i = 0; i <= 4; i++) {
                if (questions[page].answers[i]["correct"] === true) {
                    $("#answer:nth-child(" + Number(i + 1) + ")").removeClass("btn-secondary");
                    $("#answer:nth-child(" + Number(i + 1) + ")").addClass("btn-success");
                    break;
                }
            }
        }
        //estää nappien käytön
        $("button[id='answer']").attr("disabled", true);
        //selvittää oliko kysymys viimeinen ja tulostaa seuraava tai tulos napin
        if (Number(page + 1) !== questions.length) {
            $("#next-btn").removeClass("hide");
        } else {
            $("#next-btn").addClass("hide");
            $("#result-btn").removeClass("hide");
        }
    });
    //matikkatehtävän aloitus nappi
    $("#start-btn-math").click(function () {
        page++;
        $(this).addClass("hide");
        $("#start_text2").addClass("hide");
        $("#options").addClass("hide");
        $("#arrows2").addClass("hide");
        $("#math_input").removeClass("hide");
        $("#check_answer").removeClass("hide");
        generateMath();
    });
    $("#check_answer").click(function () {
        //tarkistaa onko vastaus annettu
        if ($("#answer_math").val() === "") {
            $("#answer_math").focus();
            return;
        } else {
            $("button[id='check_answer']").attr("disabled", true);
            if (Number($("#answer_math").val()) === X) {
                right++;
                $("#answer_math").addClass("green");
            } else {
                wrong++;
                $("#answer_math").addClass("red");
                $("#correct_answer").html("Oikea vastaus oli " + X); 
            }
        }
        if (page === Number($("input[name='opt2']:checked").val())) {
            setTimeout(function () {
                printResult();
            }, 2000);
        } else {
            page++;
            setTimeout(function () {
                generateMath();
            }, 2000);
        }
    });
});
//tulostaa kysymyksen ja siihen liittyvät tiedot
function printQuestion() {
    $("#info").html("Kysymys " + Number(page + 1) + " / " + questions.length);
    $("#question").html(questions[page].question);
    $("#answer-buttons").html("");
    $("#explanation").html("");
    for (i = 0; i <= 4; i++) {
        $("#answer-buttons").append('<button id="answer" class="btn btn-secondary col-12 col-sm-5" value="' + i + '">' + questions[page].answers[i]["text"] + '</button>');
    }
    page++;
}

const icons = ["+", "-", "*"];

//tulostetaan matikkatehtävä
function generateMath() {
    $("#info2").html("Lasku " + page + " / " + $("input[name='opt2']:checked").val());
    $("#calculation").html("");
    $("#answer_math").val("");
    $("#correct_answer").html("");
    $("#answer_math").removeClass("green red");
    $("#answer_math").focus();
    $("button[id='check_answer']").attr("disabled", false);
    //otetaan numerot radio buttoneista
    num1 = getRndInteger(1, $("input[name='opt1']:checked").val());
    num2 = getRndInteger(1, $("input[name='opt1']:checked").val());
    ;
    //satunnaisesti icons arraysta +, - tai *
    merkki = getRndInteger(0, 3);
    //kysytään satunnaista numeroa
    X = getRndInteger(0, 3);
    switch (merkki) {
        case 0:
            vastaus = num1 + num2;
            break;
        case 1:
            vastaus = num1 - num2;
            break;
        case 2:
            vastaus = num1 * num2;
            break;
    }
    //merkitään kysyttävä numero X:llä, molemmat jos num1 ja num2 on sama numero
    if (X === 0) {
        X = num1;
        //!== 0 ettei X - X = 0
        if (num1 === num2 && vastaus !== 0) {
            $("#calculation").append("X " + icons[merkki] + " X" + " = " + vastaus);
        } else {
            $("#calculation").append("X " + icons[merkki] + " " + num2 + " = " + vastaus);
        }
    } else if (X === 1) {
        X = num2;
        if (num1 === num2 && vastaus !== 0) {
            $("#calculation").append("X " + icons[merkki] + " X" + " = " + vastaus);
        } else {
            $("#calculation").append(num1 + " " + icons[merkki] + " X" + " = " + vastaus);
        }
    } else {
        X = vastaus;
        $("#calculation").append(num1 + " " + icons[merkki] + " " + num2 + " = X");
    }
}

//satunnaisluku funktio
function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}
function calcPercentage() {
    return right / Number($("input[name='opt2']:checked").val()) * 100;
}
function printResult() {
    $("#info2").addClass("hide");
    $("#math_input").addClass("hide");
    $("#check_answer").addClass("hide");
    $("#correct_answer").addClass("hide");
    rightPerc = calcPercentage();
    console.log(calcPercentage());
    if (rightPerc === 100) {
        $("#popUp").removeClass("hide");
        $("#popUp").modal("show");
        $("#video").get(0).play();
    } else if (rightPerc < 100 && rightPerc >= 80) {
        $("#palkinto").html('<img src="img/sydan.gif" alt=""/>');
    } else if (rightPerc < 80 && rightPerc >= 60) {
        $("#palkinto").html('<img src="img/patric.gif" alt=""/>');
    } else if (rightPerc < 60 && rightPerc >= 40) {
        $("#palkinto").html('<img src="img/kissa.gif" alt=""/>');
    } else if (rightPerc < 40 && rightPerc >= 20) {
        $("#palkinto").html('<img src="img/minion.gif" alt=""/>');
    } else if (rightPerc < 20 && rightPerc >= 0) {
        $("#palkinto").html('<img src="img/vaarin.gif" alt=""/>');
    }
    $("#calculation").html("Vastasit oikein " + right + " / " + Number($("input[name='opt2']:checked").val()));
    return;
}