/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {


    let jako = ["+", "-", "*"];
    let luku1 = 0;
    let lukuu2 = 0;
    let kysymysnro = 0;
    let oikeatkys = 0;
    let jakoNumber = 0;

    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }



    function tlasku() {

        luku1 = getRndInteger(1, 9);
        lukuu2 = getRndInteger(1, 9);
        jakoNumber = getRndInteger(jako.length, -1);

        if (jakoNumber === 1) {
            if (luku1 < lukuu2) {
                let apu = luku1;
                luku1 = lukuu2;
                lukuu2 = apu;
            }
        }
    }
    $("#aloita").click(function () {

        tlasku();
        $("#luku1").html(luku1);
        $("#luku2").html(lukuu2 + " = ");
        $("#kertoja").html(jako[jakoNumber]);


        $("#aloita").addClass("not_visible");
        $("#next").removeClass("not_visible");
        $("#tulos").removeClass("not_visible");
        $("#tarkistus").removeClass("not_visible");
        $("#oikeatvastaukset").removeClass("not_visible");

    });

    $("#tarkistus").click(function () {




        let vastaus = eval(luku1 + jako[jakoNumber] + lukuu2);
        let arvaus = Number($("#tulos").val());

        if (vastaus === arvaus) {
            $("#tulos").addClass("right");
            kysymysnro++;
            oikeatkys++;
            $("#oikeatvastaukset").html(oikeatkys + "/" + kysymysnro);
        } else {
            $("#tulos").addClass("wrong");
            kysymysnro++;
            $("#oikeavastaus").html("oikea vastaus on " + vastaus);
            $("#oikeatvastaukset").html(oikeatkys + "/" + kysymysnro);
        }


        $("#next").focus();
    });

    $("#next").click(function () {

        $("#luku1").html("");
        $("#kertoja").html("");
        $("#luku2").html("");
        $("#kertoja").html("");
        $("#tulos").val("");
//        $("#oikeavastaus").html("");

        $("#tulos").focus();


        if ($('#tulos').hasClass('wrong')) {
            $("#tulos").removeClass("wrong");
        } else {
            $("#tulos").removeClass("right");
        }


        tlasku();


        if (kysymysnro < 10) {
            $("#luku1").html(luku1);
            $("#luku2").html(lukuu2 + " = ");
            $("#kertoja").html(jako[jakoNumber]);

        } else {
            $("#next").addClass("not_visible");
            $("#tarkistus").addClass("not_visible");
            $("#kuvaa").addClass("not_visible");//piilottaa kuvan
            $("#uudestaan").removeClass("not_visible");

            if (oikeatkys === 10) {
                $("#popUp").modal("show");
                $("#video").get(0).play(); // play ei ole jqueryn functio joten siksi pitää hake Dom functio (get(0))
            } else if (oikeatkys === 9 || oikeatkys === 8) {
                $("#palkinto").html("Melkein täydet pisteet" + "<br>" + '<img src="img/sydan.gif" alt=""/>');
            } else if (oikeatkys === 7 || oikeatkys === 6 || oikeatkys === 5) {
                $("#palkinto").html("Yli puolet oikein" + "<br>" + '<img src="img/kissa.gif" alt=""/>   ');
            } else if (oikeatkys === 4 || oikeatkys === 3) {
                $("#palkinto").html("Hyvä" + "<br>" + '<img src="img/minion.gif" alt=""/>   ');
            } else if (oikeatkys < 3) {
                $("#palkinto").html("Nyt taidat pelleillä?" + "<br>" + '<img src="img/vaarin.gif" alt=""/> ');
            }

        }







//        
//        let luku = Number($("#luku").val());
//
//        if (luku === 1) {
//
//            $("#popUp").modal("show");
//            $("#video").get(0).play(); // play ei ole jqueryn functio joten siksi pitää hake Dom functio (get(0))

    });
    $("#uudestaan").click(function () {

        kysymysnro = 0;

        $("#aloita").removeClass("not_visible");
        $("#uudestaan").addClass("not_visible");
        $("#kuvaa").removeClass("not_visible");
        $("#oikeatvastaukset").html("");
        $("#oikeatvastaukset").html("/10");
        $("#palkinto").html('<img id="kuvaa" src="img/NiinaP_matikka.jpg" alt=""/>');
        $("#oikeavastaus").html("");
        oikeatkys = 0;
    });




});


