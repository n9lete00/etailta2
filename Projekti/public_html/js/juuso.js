// Author: Juuso Kuusisto

$(document).ready(function () {

    let kysymykset = ["#quiz1", "#quiz2", "#quiz3", "#quiz4", "#quiz5", "#quiz6",
        "#quiz7", "#quiz8"];
    let visible = 0;
    let laskuri = 0;
    let kysymysnumero = 1;


    //Määritellään valinnan -tapahtuma
    $(".valinta").click(function () {
        let arvo = $(this).attr("name");

        let valinta = "[name=" + arvo + "]";
        $(valinta).prop("disabled", true);

        let vastaus = Number($(this).val());
        if (vastaus === 1) { //jos vastaus on oikein lisätään tyylimäärittely
            $(this).parent().addClass("right");
            laskuri++; 

        } else {//jos vastaus on väärin lisätään tyylimäärittely
            $(this).parent().addClass("wrong");
            let arvo = $(this).attr("name");
            let valinta = "[name=" + arvo + "][value=1]";//määritetään muuttuja arvolle 1
            $(valinta).parent().addClass("right");//näytetään mikä olisi ollut oikea vastaus
        }

        $("#" + arvo).removeClass("invisible");
        $("#next").removeClass("invisible");//Tuodaa next -nappi näkyviin vastauksen jälkeen

    });
    
        //satunnaislukugeneraattori
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    
    $("#start").click(function () {
        visible = getRndInteger(0, kysymykset.length - 1);
        $(kysymykset[visible]).removeClass("invisible"); // tuodaan visan ensimmäinen kysymys esiin
        $("#start").addClass("invisible"); //piilottaa start-painikkeen
        $("#text1").addClass("invisible");//piilotetaan aloitusteksti
        $("#numero").html("<h3>" + "Kysymys " + kysymysnumero + "/8" + "</h3>");
    });


    //seuraava-painikkeen funktio
    $("#next").click(function () {

        $(kysymykset[visible]).addClass("invisible");//piilotetaan edellinen kysymys ja poistetaan se taulukosta
        kysymykset.splice(visible, 1);
        kysymysnumero++;

        if (kysymysnumero < 9) {
            $("#numero").html("<h3>" + "Kysymys " + kysymysnumero + "/8" + "</h3>");
        } else {
            $("#numero").html(""); 
        }

        //Visassa palkitseminen
        if (kysymykset.length === 0) { //jos kysymyksiä ei ole enää jäljellä piilotetaan seuraava next ja tuodaan again.
            $("#next").addClass("invisible");
//            $("#europe").addClass("invisible");
            $("#again").removeClass("invisible");
            $("#lopputulos").html("<h3>" + "Oikeita vastauksia " + laskuri + "/8" + "</h3>");//tulostaa oikeiden vastausten määrän 

            if (laskuri === 8) { //jos vastataan oikein 8 kysymystä
                $("#palkinto1").removeClass("invisible");
            } else if (laskuri > 5) {
                $("#palkinto2").removeClass("invisible");
                if (laskuri === 7) { //jos vastataan oikein 6-7 kysymystä
                    $("#oikein2").html(" seitsemään ");
                }
                if (laskuri === 6) {
                    $("#oikein2").html(" kuuteen ");
                }

            } else if (laskuri > 0) {//jos oikeita vastauksia on 1-5
                $("#palkinto3").removeClass("invisible");
                
                if (laskuri === 5) {
                    $("#oikein3").html(" viiteen ");
                }
                if (laskuri === 4) {
                    $("#oikein3").html(" neljään ");
                }
                if (laskuri === 3) {
                    $("#oikein3").html(" kolmeen ");
                }
                if (laskuri === 2) {
                    $("#oikein3").html(" kahteen ");
                }
                if (laskuri === 1) {
                    $("#oikein3").html(" yhteen ");
                }
            }
            if (laskuri === 0) { //jos ei yhtään oikeaa vastausta
                $("#palkinto4").removeClass("invisible");
            }
        }

        //Näyttää seuraavan kysymyksen,mikäli "kysymykset" -taulukossa on sisältöä
        if (kysymykset.length > 0) {
            visible = getRndInteger(0, kysymykset.length - 1);
            $(kysymykset[visible]).removeClass("invisible");
            $("#next").addClass("invisible");
        }

        //again-painikkeen funktio -> ladataan sivu uudelleen
        $("#again").click(function () {
            location.reload();
        });

    });
});