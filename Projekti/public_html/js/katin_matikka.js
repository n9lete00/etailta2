/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
// laskurille tarkoitetut globaalit muuttujat
    let laskee = 1;
    let oikea = 0;
    let lukum = 0;
    let pari = 0;


    //satunnaislukugeneraattori
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    //koodi aloitus painikkeelle
    $("#aloita").click(function () {
        $("#määrä").removeClass("not_visible");
        $(this).addClass("not_visible");
        $("#otsikko").addClass("not_visible");

    });
// Montako tehtävää haluaa tehdä valikko, jonka jälkeen ilmestyy seuraava painike
    $(".valinta").click(function () {
        let attribuutinArvo = $(this).attr("name");
        let valittu = "[name=" + attribuutinArvo + "]";
        $(valittu).prop("disabled", true);

        lukum = Number($(this).val());

        $("#seuraava").removeClass("not_visible");

    });
//    Seuraava painikkeen määrittelyt tuo esille tehtävä sivun

    $("#seuraava").click(function () {

        $("#määrä").addClass("not_visible");
        $(".taulu").addClass("not_visible");
        $("#kymppi").removeClass("not_visible");
        $(this).addClass("not_visible");

        pari = getRndInteger(1, 9);
        $("#luku").html(pari);

        $("#vast").select();
        $("#oikea").prop('disabled', true);
        $("#uusi").prop('disabled', true);

        $("#nro").html("<h3> " + laskee + "/" + lukum + "</h3>");

    });
// Tarkistus napin functio, vastauksesta riippuu mikä palkinto tai kannustuspuhe
    $("#tarkista").click(function () {

        let vastaus = Number($("#vast").val());

        let vast = 10 - pari; // laskukoodi sille mikä on satunnaismuuttujan pari
// silloin kun vastaus on oikea
        if (vast === vastaus) {

            $("#vast").addClass("right");
            $("#oikea").prop('disabled', true);

            oikea++;

            if (laskee < lukum) {
                 $(".oikein").removeClass("not_visible");
                 $(".oikein").addClass("visible");
                $("#uusi").prop('disabled', false);
            }
            if (laskee === lukum) {
                $("#tulos").html("<h3>HIENOA!<br>Katso kokonaistuloksesi tai aloita alusta.</h3>");
                $("#oikea").addClass("not_visible");
                 $("#yhteensä").removeClass("not_visible");
                 
            }

        }
// mutta jos vastaus onkin väärä
        if (vast !== vastaus) {
            $("#oikea").prop('disabled', false);
             
            $("#vast").addClass("wrong");

            if (laskee < lukum) {
                $(".väärin").removeClass("not_visible");
                $(".väärin").addClass("visible");
            }
            if (laskee === lukum) {
                $("#tulos").html("<h3>OI VOI!<br>Tarkista mikä olikaan oikea vastaus ja katso sitten kokonaistuloksesi tai aloita alusta.</h3>");
                $("#yhteensä").removeClass("not_visible");

            }

        }

        $(this).prop('disabled', true);
        if (laskee === lukum) {
            $("#yht").removeClass("not_visible");
            $("#tarkista").addClass("not_visible");
            $("#uusi").addClass("not_visible");
        }



    });
   // näyttää oikean vastauksen
    $("#oikea").click(function () {

        $("#tarkistus").addClass("right");
        $("#tarkistus").html(10 - pari);
        $("#uusi").prop('disabled', false);
        $(this).prop('disabled', true);
        if (laskee === lukum) {
            $(this).addClass("not_visible");
        }


    });
    //kun valitaan uusi tehtävä, niin tyhjentää silloin kaiken muun
    $("#uusi").click(function () {

        laskee++;
        $("#nro").html("<h3> " + laskee + "/" + lukum + "</h3>");
        $("#luku").html("");
        $("#vast").val("");
        $(".oikein").removeClass("visible");
        $(".oikein").addClass("not_visible");
        $(".väärin").removeClass("visible");
        $(".väärin").addClass("not_visible");
        $("#vast").removeClass("right");
        $("#vast").removeClass("wrong");
        $("#tarkistus").html("");
        $("#tarkistus").removeClass("right");
        pari = getRndInteger(1, 9);
        $("#luku").html(pari); 
        $("#vast").select();

        $("#tarkista").prop('disabled', false);
        $(this).prop('disabled', true);

    });

    $("#yhteensä").click(function () {
        
        $("#nro").html("<h3>" + oikea + "/" + lukum + "</h3>");
        $("#uudestaan").removeClass("not_visible");
        // Palkinto 
        if (oikea === lukum) {
            $("#popUp").modal("show");
            $("#video").get(0).play(); 
            
            
        } else if (oikea > 6) {
            $("#palkinto").html("HIENOA" + "<br>" + '<img src="img/sydan.gif" alt=""/>');


        } else if (oikea > 3) {
            $("#palkinto").html("HYVÄ" + "<br>" + '<img src="img/patric.gif" alt=""/>');
        } else if (oikea > 2) {
            $("#palkinto").html("HYVÄ, OSAAT JO HYVIN" + "<br>" + '<img src="img/kissa.gif" alt=""/>   ');
        } else if (oikea >= 1) {
            $("#palkinto").html("HYVÄ OSAAT  VÄHÄN JA VOIT AINA YRITTÄÄ UUDESTAAN" + "<br>" + '<img src="img/minion.gif" alt=""/>   ');
        } else if (oikea === 0) {
            $("#palkinto").html("OSAAT KUITENKIN YRITÄHÄN UUDESTAAN" + "<br>" + '<img src="img/vaarin.gif" alt=""/> ');
        }

    });
    $("#alusta").click(function () {
        location.reload();

    });
   

});