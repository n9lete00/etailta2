// author: Maria Järvinen
// date: 1.4.2020

$(document).ready(function () {

    let kysymykset = ['#kys1', '#kys2', '#kys3', '#kys4', '#kys5',
        '#kys6', '#kys7', '#kys8'];
    let visible = 0;
    let laske = 0; //oikein
    let nro = 1; //kysymysten määrä


    //satunnaislukugeneraattori
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    $("#aloita").click(function () {
        visible = getRndInteger(0, kysymykset.length - 1); //ensimmäinen kysymys näkyviin
        $(kysymykset[visible]).removeClass("not_visible");
        $(this).addClass("not_visible");
        $("#alku").addClass("not_visible");
        $("#nro").html(nro + "/8");
    });

    $(".choice").click(function () { //kun valitsee vastauksen
        let attr = $(this).attr("name");
        let seli = $(this).attr("data-content");

        let valinta = "[name=" + attr + "]";
        $(valinta).prop("disabled", true);

        let vastaus = Number($(this).val());

        if (vastaus === 1) { //jos oikein
            $(this).parent().addClass("valittu").addClass("oikein");
            laske++;
        } else { //väärin
            $(this).parent().addClass("väärin");
            let attr = $(this).attr("name");
            let valinta = "[name=" + attr + "][value=1]";
            $(valinta).parent().addClass("oikein");
        }

        $("#" + attr).removeClass("not_visible");
        $("#seuraava").removeClass("not_visible"); //seuraava nappi
        $(seli).removeClass("not_visible"); //selitys näkyviin
    });

    $("#seuraava").click(function () { //seuraava painiketta painettaessa
        $(kysymykset[visible]).addClass("not_visible"); //edellinen piiloon
        kysymykset.splice(visible, 1); //ei samaa kysymystä uudelleen


        nro++; //kysymysten määrä
        if (nro < 9) {
            $("#nro").html(nro + " / 8 ");
        } else {
            $("#nro").html("");
        }

        if (kysymykset.length === 0) { //jos kysymykset loppu
            $(this).addClass("not_visible"); //seuraava nappi piiloon
            $("#uudestaan").removeClass("not_visible"); //uudestaan nappi esille

//palkitseminen

            if (laske === 8) { //jos kaikki oikein
                $("#palkinto1").removeClass("not_visible");
            } else if (laske > 5) { //jos enemmän kuin 5 oikein
                $("#palkinto2").removeClass("not_visible");

                if (laske === 7) {
                    $("#oikein2").html(" seitsemään ");
                }
                if (laske === 6) {
                    $("#oikein2").html(" kuuteen ");
                }

            } else if (laske > 0) { //jos alle 5 oikein
                $("#palkinto3").removeClass("not_visible");

                if (laske === 5) {
                    $("#oikein3").html(" viiteen ");
                }
                if (laske === 4) {
                    $("#oikein3").html(" neljään ");
                }
                if (laske === 3) {
                    $("#oikein3").html(" kolmeen ");
                }
                if (laske === 2) {
                    $("#oikein3").html(" kahteen ");
                }
                if (laske === 1) {
                    $("#oikein3").html(" yhteen ");
                }
            }

            if (laske === 0) { //ei yhtään oikein
                $("#palkinto4").removeClass("not_visible");
            }
        }


        if (kysymykset.length > 0) {
            visible = getRndInteger(0, kysymykset.length - 1);
            $(kysymykset[visible]).removeClass("not_visible");
            $(this).addClass("not_visible");
        } else {

        }
        $(this).addClass("not_visible");

    });

    $("#uudestaan").click(function () { //lataa sivun uudelleen
        location.reload();

    });

    /* Matikka alkaa */


    let oikeinlkm = 0; //oikeiden vastausten lkm
    let esitetyt = 1; //kysymysten määrä
    let maximi = 0; //tehtävien valittu maksimimäärä
    let luku1 = 0; //arvottavat luvut
    let luku2 = 0;



    //valintanapit pois käytöstä kun valinta on tehty
    $(".choice1").click(function () {
        let attr = $(this).attr("name");
        let valinta = "[name=" + attr + "]";
        $(valinta).prop("disabled", true);

    });
    //valintanapit pois käytöstä kun valinta on tehty
    $(".choice2").click(function () {
        let attr = $(this).attr("name");
        let valinta = "[name=" + attr + "]";
        $(valinta).prop("disabled", true);
        $("#aloitus").removeClass("not_visible");
        $("#aloitus").addClass("visible");
    });

    $("#aloitus").click(function () {
        let valinta = $("[name=range]:checked"); //luetaan haluttu lukualue ja arvotaan numerot
        let max = Number($(valinta).attr("data-max"));
        luku1 = getRndInteger(1, max);
        luku2 = getRndInteger(1, max);


        let tehtävät = $("[name=valinta]:checked"); //luetaan tehtävien haluttu lukumäärä
        maximi = Number($(tehtävät).attr("data-max"));

        $("#luku1").html(luku1); //tulostetaan luvut laatikoihin
        $("#luku2").html(luku2);
        $("#kysytyt").html(esitetyt + "/" + maximi); //tulostetaan kysyttyjen lkm
        $("#tarkista").removeClass("not_visible"); // tarkista painike näkyville
        $("#vast").select(); //kohdistin vastausalueelle
        $(this).addClass("not_visible"); //aloita nappi piiloon

    });

    $("#tarkista").click(function () {
        let vastaus = Number($("#vast").val()); //luetaan oppilaan vastaus
        let summa = luku1 + luku2; //oikea vastaus

        if (vastaus === 0) {  //jos ei vastausta syötetty -> modal ikkuna virheeksi
            $("#virhe").modal("show");
        } else if (vastaus === summa) { //jos vastaus oikein
            oikeinlkm++;
            $(".palk2").removeClass("not_visible");
            $(".palk2").addClass("visible");
        } else {  //jos vastaus väärin
            $(".palk1").removeClass("not_visible");
            $(".palk1").addClass("visible");
            $("#correct").html("Harmi! Oikea vastaus olisi ollut " + summa);

        }

        $(this).addClass("not_visible"); //tarkista nappi piiloon
        $("#next").removeClass("not_visible"); //seuraava-nappi esille
        $("#next").addClass("visible");

    });




    $("#next").click(function () { //seuraava painike

        $("#tarkista").removeClass("not_visible"); //tarkista nappi esille
        $(".palk1").removeClass("visible"); //palkinnot piiloon
        $(".palk1").addClass("not_visible");
        $(".palk2").removeClass("visible");
        $(".palk2").addClass("not_visible");
        $("#correct").html("");
        $("#vast").val(""); //kohdistin vastauskenttään
        $("#vast").select();
        $(this).addClass("not_visible"); //seuraava nappi piiloon

        let valinta = $("[name=range]:checked"); //luetaan haluttu lukualue
        let max = Number($(valinta).attr("data-max"));



        if (esitetyt < maximi) {
            luku1 = getRndInteger(1, max); //uudet numerot ja summa
            luku2 = getRndInteger(1, max);

            $("#luku1").html(luku1); //tulostetaan luvut laatikoihin
            $("#luku2").html(luku2);
            esitetyt++; //lisätään kysyttyihin
            $("#kysytyt").html(esitetyt + "/" + maximi); //tulostetaan kysyttyjen lkm
        } else {
            $(".teht").removeClass("visible");
            $(".teht").addClass("not_visible");//palkinto sivu esille, muu piloon

            $(".palkitseminen").removeClass("not_visible"); //palkitseminen ja paljonko meni oikein
            $(".palkitseminen").addClass("visible");
            $("#oikeat").html("Oikein meni " + oikeinlkm + "/" + maximi);

            if (oikeinlkm === maximi) {
                $("#popUp").modal("show");
                $("#video").get(0).play(); // play ei ole jqueryn functio joten siksi pitää hake Dom functio (get(0))
            } else if (oikeinlkm < maximi && oikeinlkm > 8) {
                $("#palkinto").html("Huippua! Jatka vain harjoittelua!" + "<br>" + '<img src="img/sydan.gif" alt=""/>');
            } else if (oikeinlkm >= 7) {
                $("#palkinto").html("Hienoa! Jatka vain harjoittelua!" + "<br>" + '<img src="img/patric.gif" alt=""/>');
            } else if (oikeinlkm >= 6) {
                $("#palkinto").html("Hyvä! Jatka vain harjoittelua!" + "<br>" + '<img src="img/kissa.gif" alt=""/>');
            } else if (oikeinlkm > 1 && oikeinlkm < 6) {
                $("#palkinto").html("Harjoittelemalla tulee mestariksi! Eikun uusiksi!" + "<br>" + '<img src="img/minion.gif" alt=""/>');
            } else if (oikeinlkm === 0) {
                $("#palkinto").html("Harmin paikka, koitahan uudestaan!" + "<br>" + '<img src="img/vaarin.gif" alt=""/>');
            }            
        }
    });

    $("#again").click(function () { //lataa sivun uudelleen
        location.reload();
    });


});
