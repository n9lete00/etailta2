
$(document).ready(function () {
    let kysymykset = ["#kys1", "#kys2", "#kys3", "#kys4", "#kys5", "#kys6", "#kys7", "#kys8"];
    let visible = 0;
    let oikeatVastaukset = 0;


    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    $("#start").click(function () {
        // mikä esille
        visible = getRndInteger(0, kysymykset.length - 1);
        $(kysymykset[visible]).removeClass("not_visible");
        $(this).addClass("not_visible"); // start-painike
        $("#esittely").addClass("not_visible");
    });

    $(".choice").click(function () {
        $(this).parent().addClass("selected");
        let attribuutinArvo = $(this).attr("name");
        let valinta = "[name=" + attribuutinArvo + "]";
        $(valinta).prop("disabled", true);
        $("#" + attribuutinArvo).show();
        let vastaus = Number($(this).val());
        if (vastaus === 1) {
            $(this).parent().addClass("selected").addClass("right");
            oikeatVastaukset++;
            $("#tulos").html("Olet vastannut oikein " + oikeatVastaukset + "/8 kysymykseen");
        } else {
            $(this).parent().addClass("wrong");
            let oikeaVastaus = $(this).parent().parent().find("[value=1]");
            $(oikeaVastaus).parent().addClass("right");
        }

        $("#" + attribuutinArvo).removeClass("not_visible");
        $("#next").removeClass("not_visible"); // seuraava painik esille

    });

    $("#next").click(function () {
        $(kysymykset[visible]).addClass("not_visible");
        kysymykset.splice(visible, 1); // poistetaan jo käytetty


        if (kysymykset.length > 0) {
            visible = getRndInteger(0, kysymykset.length - 1);
            $(kysymykset[visible]).removeClass("not_visible");
            $(this).addClass("not_visible"); // seuraava painike
        } else {
            $(this).addClass("not_visible");

        }

        if (kysymykset.length === 0) { //jos kysymyksiä ei ole enää jäljellä
            $(this).addClass("not_visible");//piilottaa seuraava-painikkeen
            $("#kuva").addClass("not_visible");//piilottaa kuvan
            $("#uudestaan").removeClass("not_visible");//tuo esille uudestaan-painikkeen
            $("#tulos").html("<h3>Oikeita vastauksia " + oikeatVastaukset + "/8</h3>");//tulostaa oikeiden vastausten määrän 

            if (oikeatVastaukset === 8) {//jos oikeita vastauksia on 8
                $("#palkinto1").removeClass("not_visible");//näyttää "palkinto1"
            } else if (oikeatVastaukset > 3) {//jos oikeita vastauksia on enemmän kuin 3
                $("#palkinto2").removeClass("not_visible");//näyttää "palkinto2"
                //tulostetaan oikea sana palkinto2-tekstiin laskurin perusteella
                if (oikeatVastaukset === 7) {
                    $("#oikein2").html(" seitsemään ");
                }
                if (oikeatVastaukset === 6) {
                    $("#oikein2").html(" kuuteen ");
                }
                if (oikeatVastaukset === 5) {
                    $("#oikein2").html(" viiteen ");
                }
                if (oikeatVastaukset === 4) {
                    $("#oikein2").html(" neljään ");
                }

            } else if (oikeatVastaukset > 0) {//jos oikeita vastauksia on 1-4
                $("#palkinto3").removeClass("not_visible");//näyttää "palkinto3"

                //tulostetaan oikea sana palkinto3-tekstiin laskurin perusteella
                if (oikeatVastaukset === 3) {
                    $("#oikein3").html(" kolmeen ");
                }
                if (oikeatVastaukset === 2) {
                    $("#oikein3").html(" kahteen ");
                }
                if (oikeatVastaukset === 1) {
                    $("#oikein3").html(" yhteen ");
                }
  
            }
            if (oikeatVastaukset === 0) { //jos ei yhtään oikeaa vastausta
                $("#palkinto4").removeClass("not_visible");//näyttää "palkinto4"
            }
        }

    });
    $("#uudestaan").click(function () {
        location.reload();//lataa sivun uudelleen
    });
});