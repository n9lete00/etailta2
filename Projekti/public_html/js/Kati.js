/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
//Määrittelyt laskurille
    let quizzes = ["#quiz1", "#quiz2", "#quiz3", "#quiz4", "#quiz5", "#quiz6", "#quiz7", "#quiz8", "#quiz9", "#quiz10"];
    let visible = 0;
    let laskuri = 0;
     let kysymysnumero = 1;
// satunnaislukugenraattorit
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    $("#aloita").click(function () {

       visible = getRndInteger(0, quizzes.length - 1);
        $(quizzes[visible]).removeClass("not_visible"); 
        $(this).addClass("not_visible"); 
        $("#otsikko").addClass("not_visible");
//       aloitusnapin koodit



    });
//Vastauksen valinta radio napille vaihtaa vastauksen taustaväriä jos on oikein
  $(".choise").click(function () {
        let attribuutinArvo = $(this).attr("name");

        let valinta = "[name=" + attribuutinArvo + "]";
        $(valinta).prop("disabled", true);

        let vastaus = Number($(this).val());
        if (vastaus === 1) {
            $(this).parent().addClass("selected").addClass("right");
            laskuri++;
            
        } else {
            $(this).parent().addClass("wrong");

            let attribuutinArvo = $(this).attr("name");
            let valinta = "[name=" + attribuutinArvo + "] [value=1]";
            $(valinta).parent().addClass("right");
        }

        $("#" + attribuutinArvo).removeClass("not_visible");
        $("#seuraava").removeClass("not_visible");
        // vastausken jälkeen ilmestyy seuraava painike

    });

    $("#seuraava").click(function () {

        $(quizzes[visible]).addClass("not_visible");
        quizzes.splice(visible, 1);
        
        kysymysnumero++;
        if (kysymysnumero < 11) {
            $("#nro").html(  "<h3>"+kysymysnumero + "/10</h3>");
        
    } else {
        
        $("#nro").html("");
    }
        // määrittelyt laskurille ja palkinnot
        if (quizzes.length === 0){ 
            $(this).addClass("not_visible");
            $("#kuva").addClass("not_visible");
            $("#uudestaan").removeClass("not_visible");
            $("#tulos").html("<h3>Oikeita vastauksia " + laskuri + "/10</h3>");
            
            if (laskuri === 10){
                $("#palkinto1").removeClass("not_visible");
            }
            else if (laskuri > 5) {
               $("#palkinto2").removeClass("not_visible");
               
               if (laskuri === 9){
                   $("#oikein2").html(" yhdeksään ");
               }
               if (laskuri === 8) {
                   $("#oikein2").html(" kahdeksaan ");
               }
               if (laskuri === 7) {
                   $("#oikein2").html(" seitsemään ");
               }
               if (laskuri === 6) {
                   $("#oikein2").html(" kuuteen ");
               }
               
            }
            else if (laskuri > 0) {
                $("#palkinto3").removeClass("not_visible");
                
                if (laskuri === 5) {
                    $("#oikein3").html(" viiteen ");
                }
                if (laskuri === 4) {
                    $("#oikein3").html(" neljään ");
                }
                if (laskuri === 3) {
                    $("#oikein3").html(" kolmeen ");
                }
                if (laskuri === 2) {
                    $("#oikein3").html(" kahteen ");
                }
                if (laskuri === 1) {
                    $("#oikein3").html(" yhteen ");
                }
            }
            if (laskuri === 0) {
                $("#palkinto4").removeClass("not_visible");
            }
        }
        
    

        if (quizzes.length > 0) {

            visible = getRndInteger(0, quizzes.length - 1);
            $(quizzes[visible]).removeClass("not_visible");
            $(this).addClass("not_visible");
            
        } else {
            
            $(this).addClass("not_visible");
        }

    });
    
    $("#uudestaan").click(function(){ //lataa sivun uudelleen
        location.reload();
        
    });
    
     






});