/* 
 * Teija Lämpsä
 */

$(document).ready(function () {

    let laskuri = 1;//kysymysnumero/tehtävälkm
    let oikeat = 0;//oikeat vastaukset/tehtävälkm
    let tehtävälkm = 0;//tehtävien määrä
    let kertotl = 0;//valittu kertotaulu
    let kerroin = 0;//satunnaisluku


    //satunnaislukugeneraattori
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    //aloita-painikkeen funktio
    $("#aloita").click(function () {
        $("#valinnat").removeClass("not_visible"); //tuo esille kertotaulun ja tehtävämäärän valinnat
        $(this).addClass("not_visible"); //piilottaa Aloita-painikkeen
        $("#otsikko").addClass("not_visible");//piilottaa esittelytekstin
    });

    //radio-painikkeiden funktiot
    $(".choise").click(function () {
        let attribuutinArvo = $(this).attr("name");//määritetään muuttuja valitun painikkeen attribuutilla
        let valinta = "[name=" + attribuutinArvo + "]";//määritetään muuttuja "valinta" em. arvolla
        $(valinta).prop("disabled", true);//käytetään muuttujaa "valinta" poistamaan painikkeet, joilla em. attribuutti, käytöstä

        kertotl = Number($(this).val());//hakee valitun painikkeen arvon muuttujaan "kerroin"    
    });

    $(".choise2").click(function () {

        let attribuutinArvo = $(this).attr("name");//määritetään muuttuja valitun painikkeen attribuutilla
        let valinta2 = "[name=" + attribuutinArvo + "]";//määritetään muuttuja "valinta" em. arvolla
        $(valinta2).prop("disabled", true);//käytetään muuttujaa "valinta" poistamaan painikkeet, joilla em. attribuutti, käytöstä

        tehtävälkm = Number($(this).val());//hakee valitun painikkeen arvon muuttujaan "tehtävälkm"

        $("#seuraava").removeClass("not_visible");//näyttää seuraava-painikkeen

    });

    //seuraava-painikkeen funktio
    $("#seuraava").click(function () {

        $("#valinnat").addClass("not_visible");//piilottaa valinnat
        $("#tehtävä").removeClass("not_visible");//tuo esille tehtävä-sivun
        $(this).addClass("not_visible");//piilottaa seuraava-painikkeen
        kerroin = getRndInteger(1, 10);//satunnaisluku muuttujaan kerroin
        $("#laskutehtävä").html(kerroin + "<span class='x'>" + " X " + "</span>" + kertotl + " = ");//tulostaa sivulle tehtävän
        $("#nro").html("<h3>Kysymys " + laskuri + "/" + tehtävälkm + "</h3>");//tulostaa sivulle laskurin
        $("#usr").select();//vie kohdistimen syöttökenttään
        $("#oikea").prop('disabled', true);//poistaa painikkeen "oikea vastaus" käytöstä
        $("#uusi").prop('disabled', true);//poistaa painikkeen "uusi" käytöstä

    });
    //tarkista-painikkeen funktio
    $("#tarkista").click(function () {

        let vastaus = Number($("#usr").val());//syöte muuttujaan "vastaus"

        $("#vastaus").html(vastaus);//tulostaa muuttujan "vastaus"

        let cal = (kerroin * kertotl);//laskee muuttujan "cal"


        if (cal === vastaus) {//jos vastaus on oikein

            $("#vastaus").addClass("right");//lisää luokan "right"
            $("#oikea").prop('disabled', true);//poistaa painikkeen "oikea vastaus" käytöstä

            oikeat++;//+1 muuttujaan "oikeat"

            if (laskuri < tehtävälkm) {//jos tehtäviä vielä jäljellä
                $("#tulos").html("<h3>Hienoa, vastasit oikein!<br>Valitse uusi tehtävä.</h3>");//tulostus
                $("#uusi").prop('disabled', false);//tuo painikkeen "uusi" käyttöön
            }
            if (laskuri === tehtävälkm) {//jos viimeinen tehtävä
                $("#tulos").html("<h3>Hienoa, vastasit oikein!<br>Katso kokonaistuloksesi tai aloita alusta.</h3>");//tulostus
                $("#oikea").addClass("not_visible");//poistaa painikkeen "oikea" käytöstä

            }

        }

        if (cal !== vastaus) {//jos vastaus on väärin
            $("#oikea").prop('disabled', false);//painike "oikea vastaus" käyttöön
            $("#vastaus").addClass("wrong");//lisää luokan "wrong" 

            if (laskuri < tehtävälkm) {//jos tehtäviä vielä jäljellä
                $("#tulos").html("<h3 class='wrong'>Hups! Väärin meni.<br>Tarkista mikä olikaan oikea vastaus ja valitse sitten uusi tehtävä.</h3>");
            }
            if (laskuri === tehtävälkm) {//jos viimeinen tehtävä
                $("#tulos").html("<h3 class='wrong'>Hups! Väärin meni.<br>Tarkista mikä olikaan oikea vastaus ja katso sitten kokonaistuloksesi tai aloita alusta.</h3>");


            }

        }

        $(this).prop('disabled', true);//poistaa painikkeen "tarkista vastaus" käytöstä
        if (laskuri === tehtävälkm) {
            $("#yht").removeClass("not_visible");//tuo esille "kokonaistulos" painikkeen
            $("#tarkista").addClass("not_visible");//piilottaa painikkeen "tarkista"
            $("#uusi").addClass("not_visible");//piilottaa painikkeen "uusi"
        }



    });
    //painikkeen "oikea vastaus" funktio
    $("#oikea").click(function () {

        $("#vastaus").addClass("right");//antaa vastaukselle luokan "right"(vihreä väri)
        $("#vastaus").html(kerroin * kertotl);//laskee oikean vastuksen
        $("#uusi").prop('disabled', false);//tuo painikkeen "uusi" käyttöön
        $(this).prop('disabled', true);//poistaa painikkeen "oikea vastaus" käytöstä
        if (laskuri === tehtävälkm) {
            $(this).addClass("not_visible");
        }


    });
    //painikkeen "uusi" funktio
    $("#uusi").click(function () {

        laskuri++;//+1 muuttujaan "laskuri"
        $("#nro").html("<h3>Kysymys " + laskuri + "/" + tehtävälkm + "</h3>");//tulostaa sivulle tehtävälaskurin
        $("#tulos").html("");//tyhjentää elementin "tulos"
        $("#usr").val("");//tyhjentää syöttökentän

        $("#vastaus").html("");//tyhjentää tulostuksen elementissä "vastaus"
        $("#vastaus").removeClass("right");//poistaa luokan "right" elementiltä "vastaus"
        kerroin = getRndInteger(1, 10);//satunnaisluku väliltä 1-10
        $("#laskutehtävä").html(kerroin + "<span class='x'>" + " X " + "</span>" + kertotl + " = ");//tulostaa sivulle tehtävän
        $("#usr").select();//kohdistin syöttökenttään

        $("#tarkista").prop('disabled', false);//"tarkista vastaus" painike käyttöön
        $(this).prop('disabled', true);//"uusi tehtävä" painike pois käytöstä

    });

    $("#yht").click(function () {
        $("#tehtävä").addClass("not_visible");//piilottaa tehtäväsivun
        $("#nro").html("<h3>Tuloksesi on " + oikeat + "/" + tehtävälkm + "</h3>");//tulostaa kokonaistuloksen
        $("#uudestaan").removeClass("not_visible");

        if (oikeat === tehtävälkm) {
            $("#popUp").modal("show");
            $("#video").get(0).play(); // play ei ole jqueryn functio joten siksi pitää hake Dom functio (get(0))
        } else if (oikeat > 10) {
            $("#palkinto").html("<h1>Mahtava suoritus!</h1><br>Olet oikea matikkamonsteri!<br>Sinulle saattaa kuitenkin löytyä vielä haastavampi kertotaulu.<br>Kokeile siis ihmeessä uudestaan!" + "<br>" + '<img src="img/sydan.gif" alt=""/>');
        } else if (oikeat > 8) {
            $("#palkinto").html("<h1>Upeaa!</h1><br>Kertotaulut on kyllä mahtava juttu!.<br>Ehkä voisit pienellä harjoittelulla päästä täydelliseen suoritukseen?.<br>Kokeile siis ihmeessä uudestaan!" + "<br>" + '<img src="img/patric.gif" alt=""/>');
        } else if (oikeat > 6) {
            $("#palkinto").html("<h1>Oletpa etevä!</h1><br>Teit oikein loisto suorituksen.<br>Aina voit kuitenkin oppia lisää.<br>Kokeile siis ihmeessä uudestaan!" + "<br>" + '<img src="img/kissa.gif" alt=""/>   ');
        } else if (oikeat > 2) {
            $("#palkinto").html("<h1>Harjoitus tekee mestarin!</h1><br>Sinnikkäästi jaksat yrittää ja aina voit oppia lisää.<br>Kokeile siis ihmeessä uudestaan!" + "<br>" + '<img src="img/minion.gif" alt=""/>   ');
        } else if (oikeat >= 0) {
            $("#palkinto").html("<h1>Hyvä yritys!</h1><br>Enemmän kuitenkin meni väärin kuin oikein.<br>Olemalla sinnikäs voi aina oppia lisää.<br>Kokeile siis ihmeessä uudestaan!" + "<br>" + '<img src="img/vaarin.gif" alt=""/> ');
        }

    });

    //"aloita alusta"-painikkeen funktio
    $("#alusta").click(function () {
        location.reload();//lataa sivun uudelleen

    });
    $("#uudestaan").click(function () {
        location.reload();//lataa sivun uudelleen 
    });


});

