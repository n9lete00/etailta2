// Author: Juuso Kuusisto


$(document).ready(function () {

    //satunnaislukugeneraattori
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    let right_answers = 0; //laskee oikeat vastaukset
    let task = 1; //Globaali muuttuja, jolla lasketaan monesko lasku menossa
    let quiz_length = 0; //määrittää visan pituuden
    let num1 = 0;
    let num2 = 0;
    
    //määritetään funktio, jolla määritetään mitkä luvut tulostuvat laskulle
    function MathRange() {
        let selection = Number($("[name=numbers]:checked").val()); //Määritetään arvo radio buttonista
        if (selection === 10) {  // jos value = 1 numeroalue: 1-10
            num1 = getRndInteger(1, 10);
            num2 = getRndInteger(1, num1); //toinen parametri num1, jotta ei tule negatiivista tulosta
        }
        if (selection === 20) {
            num1 = getRndInteger(1, 20);
            num2 = getRndInteger(0, num1);
        }
        if (selection === 30) {
            num1 = getRndInteger(1, 30);
            num2 = getRndInteger(0, num1);
        }
        if (selection === 40) {
            num1 = getRndInteger(1, 40);
            num2 = getRndInteger(0, num1);
        }
    }

    $("#start").click(function () {
        quiz_length = Number($("[name=amounts]:checked").val()); // luetaan visan pituus (value) "määrät"-valintanapeista
        $("#go").addClass("invisible"); //piilotetaan aloita -painike
        $("#nro_range").addClass("invisible"); //piilotetaan numeroalueen valintanapit
        $("#q_range").addClass("invisible"); //piilotetaan laskujen määrän valintanapit

        $("#answer").removeClass("invisible"); // Tuo vastaus-laatikon näkösälle
        $("#answer").focus(); // vie kursorin suoraan vastauslaatikkoon
        MathRange(); // kutsutaan lukualue-funktiota

        $("#calculation").html(num1 + " - " + num2 + " = "); //tulostaa laskun
        $("#nro").html("<h3>Lasku " + task + "/" + quiz_length + "</h3>"); // tulostaa sivulle nro/5 tai nro/10 tai nro/15
        $("#check").removeClass("invisible"); //tuo esille vastaa -napin
        task ++;
    });


    $("#check").click(function () {
        let answer = Number($("#answer").val()); //lukee vastauslaatikkoon syötetyn numeron
        $("#check").addClass("invisible"); //piilotetaan "tarkista" -nappi
        $("#next_q").removeClass("invisible"); //tuo esille "eteenpäin" -nappi

        let result = num1 - num2;  //laskutoimitus
        if (answer === result) {
            //jos vastaus oikein
            $("#print").html("<h3>Oikein! Mainiota!</h3>");
            $("#answer").addClass("right"); //Vastaus vihreäksi
            right_answers++;
        } else {
            //jos vastaus oikein
            $("#print").html("<h4>Oikea vastaus olisi ollut " + result + "." + "<br>Tsemppiä seuraavaan tehtävään!</h4>");
            $("#answer").addClass("wrong"); // vastaus punaiseksi
        } 
    });


    $("#next_q").click(function () {
        $("#answer").val(""); //tyhjennetään vastaus-kenttä
        $("#calculation").html(""); // tyhjentää laskun tulostuksen
        $("#print").html(""); // tyhjentää tulostuksen
        $("#answer").focus();  //Viedään kursori vastaus-kenttään

        // Seuraavaksi määritetään visan pituus
        if (task  <= quiz_length) {
            MathRange();
            $("#calculation").html(num1 + " - " + num2 + " = "); //tulostetaan uusi lasku jos semmoinen jäljellä
            $("#nro").html("<h3>Lasku " + task + "/" + quiz_length + "</h3>");
            $("#answer").removeClass("right"); 
            $("#answer").removeClass("wrong");  
            $("#check").removeClass("invisible"); //tuodaan "tarkista" -nappi esiin
            $("#next_q").addClass("invisible"); //tuodaan "eteenpäin" -nappi esiin
            task ++;
        } else {
            $("#answer").addClass("invisible"); //piilotetaan "tarkista" -nappi
            $("#next_q").addClass("invisible"); //piilotetaan "tarkista -napin
            $("#correct_answers").removeClass("invisible"); //tuodaan esiin oikeiden vastausten määrä
            $("#nro").addClass("invisible"); //piilotetaan laskuri
            $("#test_result").removeClass("invisible"); //tuodaan "lopputulos" -nappi esiin
        } 
    });
    
    //Palkitsemiset:
    //Author: Niina Panula
    //Modifoitu omaan käyttöön: Juuso Kuusisto
    $("#test_result").click(function () {
        $("#correct").html("Vastasit oikein " + right_answers + " / " + quiz_length + " tehtävään."); // Näyttää oikein laskettujen laskujen määrän
        $("#again").removeClass("invisible"); //Tuodaan "again" -button esiin
        $("#test_result").addClass("invisible"); //piilotetaan "lopputulos" -nappi

        //Jos vastauksia x-määrä oikein näytetetään tietty kuva
        if (right_answers === quiz_length) {
            $("#popUp").modal("show"); // paras tulos näytetään modal-ikkunassa
            $("#video").get(0).play(); // play ei ole jqueryn functio joten siksi pitää hake Dom functio (get(0))
        } else if (right_answers > 10) {
            $("#prize").html("<h1>Wau!</h1><br>Haluatko yrittää vielä parantaa tulosta?" + "<br>" + '<img src="img/sydän.gif" alt=""/>');
        } else if (right_answers > 8) {
            $("#prize").html("<h1>Loistavaa!</h1><br>Lähes kaikki kysymykset oikein! Tavoitellaanko seuraavaksi täydellisyyttä?" + "<br>" + '<img src="img/patric.gif" alt=""/>');
        } else if (right_answers > 6) {
            $("#prize").html("<h1>Mainiota!</h1><br>Suorituksesi oli todella hyvä. Haluatko vielä parantaa?" + "<br>" + '<img src="img/kissa.gif" alt=""/>');
        } else if (right_answers > 2) {
            $("#prize").html("<h1>Hyvä suoritus!</h1><br>Harjoitus tekee mestarin, joten kokeilethan uudelleen!" + "<br>" + '<img src="img/minion.gif" alt=""/>');
        } else if (right_answers >= 0) {
            $("#prize").html("<h1>Hyvä yritys!</h1><br>Tällä kertaa vastasit lähes \n\
            kaikkiin väärin. Kokeilethan uudelleen!" + "<br>" + '<img src="img/vaarin.gif" alt=""/> ');
        }
    });

    //again-painikkeen funktio -> ladataan sivu uudelleen
    $("#again").click(function () {
        location.reload();
    });

});