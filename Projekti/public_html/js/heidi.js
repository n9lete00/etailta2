/* 
 * Heidi Risto
 */
$(document).ready(function () {
    let visa = ['#kysymys1', '#kysymys2', '#kysymys3', '#kysymys4', '#kysymys5', '#kysymys6', '#kysymys7', '#kysymys8'];
    let visible = 0;
    let laskuri = 0; // laskee oikeat vastaukset
    let kysymysnumero = 1;//kysymysnumero alottaa ykkösestä

    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    $("#aloita").click(function () {
        // mikä esille
        visible = getRndInteger(0, visa.length - 1);
        $(visa[visible]).removeClass("piilossa");
        $(this).addClass("piilossa");  // piilotetaan aloitus-painike
        $(".otsikko").addClass("piilossa");  // piilotetaan myös otsikko
        $(".saate").addClass("piilossa");  // piilotetaan myös saatetekstit
        $("#nro").html("<h3>Kysymys " + kysymysnumero + "/8</h3>"); // tulostaa sivulle nro/8

    });

    $(".kysymys").click(function () {
        // mikä valintanappiryhmä?
        let attribuutinArvo = $(this).attr("name");

        // ei saa valita uudestaan
        let valinta = "[name=" + attribuutinArvo + "]";
        $(valinta).prop("disabled", true);

        // onko oikein
        let vastaus = Number($(this).val());
        if (vastaus === 1) {
            // oikea vastaus
            $(this).parent().addClass("valittu").addClass("oikein");
            laskuri++;
        } else {
            $(this).parent().addClass("väärin");
            // [name=xxx] [value=1]
            let attribuutinArvo = $(this).attr("name");
            let valinta = "[name=" + attribuutinArvo + "][value=1]";
            $(valinta).parent().addClass("oikein");
        }

        $("#" + attribuutinArvo).removeClass("piilossa"); //näytetään selitys
        $("#seuraava").removeClass("piilossa"); // seuraava painike esille
    });

    $("#seuraava").click(function () {
        $(visa[visible]).addClass("piilossa");
        visa.splice(visible, 1);  // poistetaan jo näytetty

        kysymysnumero++;//kysymysnumero +1
        if (kysymysnumero < 9) {//kun muuttujan "kysymysnumero" arvo on alle 9
            $("#nro").html("<h3>Kysymys " + kysymysnumero + "/8</h3>");//tulostetaan nro/8 sivulle
        } else {//kun "kysymysnumero" on yli 8
            $("#nro").html(""); //tyhjentää kysymysnumero tulostuksen
        }

        if (visa.length === 0) { // kun visan pituus on 0
            $(this).addClass("piilossa");//piilottaa seuraava-painikkeen
            $("#tulos").html("<h3>Oikeita vastauksia " + laskuri + "/8</h3>");//tulostaa oikeiden vastausten määrän
            $("#nro").addClass("piilossa"); // piilotetaan myös laskuri tilaa viemästä

            if (laskuri === 8) {//jos oikeita vastauksia on 8
                $("#palkinto1").removeClass("piilossa");//näyttää "palkinto1"
            } else if (laskuri > 4) {//jos oikeita vastauksia on enemmän kuin 4
                $("#palkinto2").removeClass("piilossa");//näyttää "palkinto2"
                //tulostetaan oikea sana palkinto2-tekstiin laskurin perusteella
                if (laskuri === 7) {
                    $("#oikein2").html(" seitsemään ");
                }
                if (laskuri === 6) {
                    $("#oikein2").html(" kuuteen ");
                }
                if (laskuri === 5) {
                    $("#oikein2").html(" viiteen ");
                }
                
            } else if (laskuri > 0) {//jos oikeita vastauksia on 1-3
                $("#palkinto3").removeClass("piilossa");//näyttää "palkinto3"
                //tulostetaan oikea sana palkinto3-tekstiin laskurin perusteella
                if (laskuri === 4) {
                    $("#oikein3").html(" neljään ");
                }
                if (laskuri === 3) {
                    $("#oikein3").html(" kolmeen ");
                }
                if (laskuri === 2) {
                    $("#oikein3").html(" kahteen ");
                }
                if (laskuri === 1) {
                    $("#oikein3").html(" yhteen ");
                }
            }

            if (laskuri === 0) { //jos ei yhtään oikeaa vastausta
                $("#palkinto4").removeClass("piilossa");//näyttää "palkinto4"
            }
        }

        // vieläkö on näytettävää
        if (visa.length > 0) {
            // mikä esille
            visible = getRndInteger(0, visa.length - 1);
            $(visa[visible]).removeClass("piilossa");
            $(this).addClass("piilossa");  // seuraava painike
        }
    });
});


