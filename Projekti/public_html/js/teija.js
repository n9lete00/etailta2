/* 
 * Teija Lämpsä
 */

$(document).ready(function () {

    let quizzes = ["#quiz1", "#quiz2", "#quiz3", "#quiz4", "#quiz5", "#quiz6", "#quiz7", "#quiz8", "#quiz9", "#quiz10"];
    let visible = 0; //näytetyt kysymykset
    let laskuri = 0; //oikeat vastaukset
    let kysymysnumero = 1;//kysymysnumero/10

    //satunnaislukugeneraattori
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    //aloita-painikkeen funktio
    $("#aloita").click(function () {

        visible = getRndInteger(0, quizzes.length - 1);//määritetään satunnaisluku muuttujaan "visible" väliltä 0 - quizzes-taulukon pituus - 1
        $(quizzes[visible]).removeClass("not_visible"); //valitsee visan ja tuo sen esille
        $(this).addClass("not_visible"); //piilottaa Aloita-painikkeen
        $("#otsikko").addClass("not_visible");//piilottaa esittelytekstin
        $("#nro").html("<h3>Kysymys " + kysymysnumero + "/10</h3>"); // tulostaa sivulle 1/10


    });
    //radio-painikkeen funktio
    $(".choise").click(function () {
        let attribuutinArvo = $(this).attr("name");//määritetään muuttuja valitun painikkeen attribuutilla

        let valinta = "[name=" + attribuutinArvo + "]";//määritetään muuttuja "valinta" em. arvolla
        $(valinta).prop("disabled", true);//käytetään muuttujaa "valinta" poistamaan painikkeet, joilla em. attribuutti, käytöstä

        let vastaus = Number($(this).val());//hakee valitun painikkeen arvon muuttujaan "vastaus"
        if (vastaus === 1) {//jos arvo on 1
            $(this).parent().addClass("selected").addClass("right");//annetaan valinnalle class="right" (vihreä väri)
            laskuri++;//lisää oikean vastauksen laskuriin
            
        } else {//jos arvo ei ole 1
            $(this).parent().addClass("wrong");//annetaan valinnalle class="wrong" (punainen väri)

            let attribuutinArvo = $(this).attr("name");//määritetään muuttuja valitun painikkeen attribuutilla
            let valinta = "[name=" + attribuutinArvo + "][value=1]";//määritetään muuttuja jolla on value="1" "valinta" em. arvolla
            $(valinta).parent().addClass("right");//lisätään class="right" (vihreä väri)
        }

        $("#" + attribuutinArvo).removeClass("not_visible");//näyttää selitteen jolla on sama attribuutti kuin valitulla visalla
        $("#seuraava").removeClass("not_visible");//näyttää seuraava-painikkeen

    });
    //seuraava-painikkeen funktio
    $("#seuraava").click(function () {

        $(quizzes[visible]).addClass("not_visible");//piilottaa jo näytetyn visan
        quizzes.splice(visible, 1);//poistaa jo näytetyn visan taulukosta
        
        kysymysnumero++;//kysymysnumero +1
        if (kysymysnumero < 11) {//kun muuttujan "kysymysnumero" arvo on alle 11
        $("#nro").html("<h3>Kysymys " + kysymysnumero + "/10</h3>");//tulostetaan kysymysnumero/10 sivulle
        
    } else {//kun "kysymysnumero" on yli 11
        
        $("#nro").html(""); //tyhjentää kysymysnumero tulostuksen
    }
        
        if (quizzes.length === 0){ //jos kysymyksiä ei ole enää jäljellä
            $(this).addClass("not_visible");//piilottaa seuraava-painikkeen
            $("#kuva").addClass("not_visible");//piilottaa kuvan
            $("#uudestaan").removeClass("not_visible");//tuo esille uudestaan-painikkeen
            $("#tulos").html("<h3>Oikeita vastauksia " + laskuri + "/10</h3>");//tulostaa oikeiden vastausten määrän 
            
            if (laskuri === 10){//jos oikeita vastauksia on 10
                $("#palkinto1").removeClass("not_visible");//näyttää "palkinto1"
            }
            else if (laskuri > 5) {//jos oikeita vastauksia on enemmän kuin 5
               $("#palkinto2").removeClass("not_visible");//näyttää "palkinto2"
               //tulostetaan oikea sana palkinto2-tekstiin laskurin perusteella
               if (laskuri === 9){
                   $("#oikein2").html(" yhdeksään ");
               }
               if (laskuri === 8) {
                   $("#oikein2").html(" kahdeksaan ");
               }
               if (laskuri === 7) {
                   $("#oikein2").html(" seitsemään ");
               }
               if (laskuri === 6) {
                   $("#oikein2").html(" kuuteen ");
               }
               
            }
            else if (laskuri > 0) {//jos oikeita vastauksia on 1-4
                $("#palkinto3").removeClass("not_visible");//näyttää "palkinto3"
                
                //tulostetaan oikea sana palkinto3-tekstiin laskurin perusteella
                if (laskuri === 5) {
                    $("#oikein3").html(" viiteen ");
                }
                if (laskuri === 4) {
                    $("#oikein3").html(" neljään ");
                }
                if (laskuri === 3) {
                    $("#oikein3").html(" kolmeen ");
                }
                if (laskuri === 2) {
                    $("#oikein3").html(" kahteen ");
                }
                if (laskuri === 1) {
                    $("#oikein3").html(" yhteen ");
                }
            }
            if (laskuri === 0) { //jos ei yhtään oikeaa vastausta
                $("#palkinto4").removeClass("not_visible");//näyttää "palkinto4"
            }
        }
        
    

        if (quizzes.length > 0) {//jos quizzes-taulukossa on vielä sisältöä

            visible = getRndInteger(0, quizzes.length - 1);//arvotaan seuraava
            $(quizzes[visible]).removeClass("not_visible");//tuodaan se esille
            $(this).addClass("not_visible");//piilottaa seuraava-painikkeen
            
        } 
        
    });
    
    //uudestaan-painikkeen funktio
    $("#uudestaan").click(function(){ 
        location.reload();//lataa sivun uudelleen
        
    });




});

