/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {

    let kysymyksetMix = ["#quiz1", "#quiz2", "#quiz3", "#quiz4", "#quiz5", "#quiz6", "#quiz7", "#quiz8"];

    let laskuri = 0; //oikeat vastaukset
    let kysymysnumero = 0;//kysymysnumero/10
    let kysymyksetShow = 0;

    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }




    $("#start").click(function () {

        kysymyksetShow = getRndInteger(0, kysymyksetMix.length - 1);
        $(kysymyksetMix[kysymyksetShow]).removeClass("not_visible");
        $(this).addClass("not_visible");
        $("#next").parent().removeClass("not_visible");


//        
    });

    $(".choise").click(function () {

        let vastaus = Number($(this).val());
        if (vastaus === 1) {
            $(this).parent().addClass("selected").addClass("right");
            laskuri++;
            kysymysnumero++;
            $("#laskeminen").html(kysymysnumero + "/8");



        } else {
            $(this).parent().addClass("wrong");
            let oikeaVastaus = $(this).parent().parent().find("[value=1]");
            oikeaVastaus.parent().addClass("right");
            kysymysnumero++;
            $("#laskeminen").html(kysymysnumero + "/8");


        }

//        
        $(this).parent().addClass("selected");

        let atribuutinArvo = $(this).attr("name");

        let valinta = "[name=" + atribuutinArvo + "]";
        $(valinta).prop("disabled", true);
        $("#" + atribuutinArvo).show();



    });



    $("#next").click(function () {



        $(kysymyksetMix[kysymyksetShow]).addClass("not_visible");
        kysymyksetMix.splice(kysymyksetShow, 1);

        if (kysymyksetMix.length > 0) {

            kysymyksetShow = getRndInteger(0, kysymyksetMix.length - 1);
            $(kysymyksetMix[kysymyksetShow]).removeClass("not_visible");

        }else  { //jos kysymyksiä ei ole enää jäljellä
            $(this).addClass("not_visible");//piilottaa seuraava-painikkeen
            $("#kuva").addClass("not_visible");//piilottaa kuvan
            $("#uudestaan").removeClass("not_visible");//tuo esille uudestaan-painikkeen
            $("#tulos").html("<h3>Oikeita vastauksia " + laskuri + "/8</h3>");//tulostaa oikeiden vastausten määrän 

            if (laskuri === 8) {//jos oikeita vastauksia on 10
                $("#palkinto1").removeClass("not_visible");//näyttää "palkinto1"
            } else if (laskuri > 5) {//jos oikeita vastauksia on enemmän kuin 5
                $("#palkinto2").removeClass("not_visible");//näyttää "palkinto2"
                //tulostetaan oikea sana palkinto2-tekstiin laskurin perusteella

                if (laskuri === 8) {
                    $("#oikein2").html(" kahdeksaan ");
                }
                if (laskuri === 7) {
                    $("#oikein2").html(" seitsemään ");
                }
                if (laskuri === 6) {
                    $("#oikein2").html(" kuuteen ");
                }

            } else if (laskuri > 0) {//jos oikeita vastauksia on 1-4
                $("#palkinto3").removeClass("not_visible");//näyttää "palkinto3"

//                tulostetaan oikea sana palkinto3-tekstiin laskurin perusteella
                if (laskuri === 5) {
                    $("#oikein3").html(" viiteen ");
                }
                if (laskuri === 4) {
                    $("#oikein3").html(" neljään ");
                }
                if (laskuri === 3) {
                    $("#oikein3").html(" kolmeen ");
                }
                if (laskuri === 2) {
                    $("#oikein3").html(" kahteen ");
                }
                if (laskuri === 1) {
                    $("#oikein3").html(" yhteen ");
                }
            }
            if (laskuri === 0) { //jos ei yhtään oikeaa vastausta
                $("#palkinto4").removeClass("not_visible");//näyttää "palkinto4"
            }


        }




    });

//    
//    
});
////    //uudestaan-painikkeen funktio
////    $("#uudestaan").click(function(){ 
////        location.reload();//lataa sivun uudelleen
//        
//    
//    
//     
//
//
//
//
//
//
//
//
